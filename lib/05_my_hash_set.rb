#Anastassia Bobokalonova
#May 23, 2017
# MyHashSet
#
# Ruby provides a class named `Set`. A set is an unordered collection of
# values with no duplicates.  You can read all about it in the documentation:
#
# http://www.ruby-doc.org/stdlib-2.1.2/libdoc/set/rdoc/Set.html
#
# Let's write a class named `MyHashSet` that will implement some of the
# functionality of a set. Our `MyHashSet` class will utilize a Ruby hash to keep
# track of which elements are in the set.  Feel free to use any of the Ruby
# `Hash` methods within your `MyHashSet` methods.
#
# Write a `MyHashSet#initialize` method which sets an empty hash object to
# `@store`. Next, write an `#insert(el)` method that stores `el` as a key
# in `@store`, storing `true` as the value. Write an `#include?(el)`
# method that sees if `el` has previously been `insert`ed by checking the
# `@store`; return `true` or `false`.
#
# Next, write a `#delete(el)` method to remove an item from the set.
# Return `true` if the item had been in the set, else return `false`.  Add
# a method `#to_a` which returns an array of the items in the set.
#
# Next, write a method `set1#union(set2)` which returns a new set which
# includes all the elements in `set1` or `set2` (or both). Write a
# `set1#intersect(set2)` method that returns a new set which includes only
# those elements that are in both `set1` and `set2`.
#
# Write a `set1#minus(set2)` method which returns a new set which includes
# all the items of `set1` that aren't in `set2`.

class MyHashSet
  attr_reader :store
  def initialize
    @store = Hash.new(false)
  end

  def insert(el)
    @store[el] = true
  end

  def include?(el)
    @store[el]
  end

  def delete(el)
    if include?(el)
      @store[el] = false
      return true
    else
      return false
    end
  end

  def to_a
    @store.keys
  end

  def union(set2)
    u_set = MyHashSet.new
    @store.each { |k, v| u_set.insert(k) }
    set2.store.each { |k, v| u_set.insert(k) }
    u_set
  end

  def intersect(set2)
    i_set = MyHashSet.new
    @store.each { |k, v| i_set.insert(k) if set2.include?(k) }
    i_set
  end

  def minus(set2)
    m_set = MyHashSet.new
    @store.each { |k, v| m_set.insert(k) unless set2.include?(k) }
    m_set
  end

  def symmetric_difference(set2)
    d_set = MyHashSet.new
    @store.each { |k, v| d_set.insert(k) unless set2.include?(k) }
    set2.store.each { |k, v| d_set.insert(k) unless @store.include?(k) }
    d_set
  end

  def ==(object)
    (object.class == MyHashSet) && (object.store.keys == @store.keys)
  end
end

# Bonus
#
# - Write a `set1#symmetric_difference(set2)` method; it should return the
#   elements contained in either `set1` or `set2`, but not both!
# - Write a `set1#==(object)` method. It should return true if `object` is
#   a `MyHashSet`, has the same size as `set1`, and every member of
#   `object` is a member of `set1`.
