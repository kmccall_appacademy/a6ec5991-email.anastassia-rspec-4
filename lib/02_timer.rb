class Timer
  def initialize
    @seconds = 0
  end

  def seconds
    @seconds
  end

  def seconds=(s)
    @seconds = s
  end

  def time_string
    hours = seconds / 3600
    minutes = (seconds - (hours * 3600)) / 60
    secs = (seconds - ((hours * 3600) + (minutes * 60)))
    "#{padded(hours)}:#{padded(minutes)}:#{padded(secs)}"
  end

  def padded(time)
    return "0#{time}" if time < 10
    time.to_s
  end
end
