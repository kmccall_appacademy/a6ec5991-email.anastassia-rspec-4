class Temperature

  def initialize(options={})
    @f = options[:f]
    @c = options[:c]
  end

  def in_celsius
    @c ? @c : (@f - 32) * (5.0 / 9.0)
  end

  def in_fahrenheit
    @f ? @f : (@c * (9.0 / 5.0)) + 32
  end

  def self.from_celsius(c)
    Temperature.new({c:c})
  end

  def self.from_fahrenheit(f)
    Temperature.new({f:f})
  end

end
