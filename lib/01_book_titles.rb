class Book
  def title
    @title
  end

  def title=(name)
    @title = caps(name)
  end

  def caps(name)
    lower_case_words = ["a", "an", "the", "of", "in", "and"]
    name.capitalize.split.map do |word|
      if lower_case_words.include?(word)
        word
      else
        word.capitalize
      end
    end.join(" ")
  end

end
