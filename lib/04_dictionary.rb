class Dictionary
  attr_accessor :entries

  def initialize
    @entries = Hash.new(nil)
  end

  def add(word = {})
    case
    when word.class == Hash
      @entries[word.keys[0]] = word.values[0]
    when word.class == String
      @entries[word] = nil
    end
  end

  def include?(word)
    keywords.include?(word)
  end

  def find(word)
    @entries.select {|k, v| k.include?(word)}
  end

  def keywords
    @entries.keys.sort
  end

  def printable
    output = ""
    @entries.sort.each do |k, v|
      output << "[#{k}] \"#{v}\"\n"
    end
    output.chomp
  end

end
